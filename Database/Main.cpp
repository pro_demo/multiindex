#include <iostream>
#include "EngineData.h"
#include "Logger.h"
void main()
{
	initLogger(debug, "", "out");

	auto &engineData = EngineData::getInstance();
	engineData.initDatabase();
	engineData.removeAll();

	Account account1, account2, account3;
	account1.xid = "zb";
	account1.uid = "gaolj";

	account2.xid = "zb";
	account2.uid = "toiletduck";

	account3.xid = "zb";
	account3.uid = "caoning";

	engineData.insertAccount(account1);
	engineData.insertAccount(account2);
	engineData.insertAccount(account3);
	engineData.insertAccount(account3);

	Order order1, order2, order3, order4;
	order1.xid = "zb";
	order1.oid = "order_1";
	order1.uid = "gaolj";

	order2.xid = "zb";
	order2.oid = "order_2";
	order2.uid = "gaolj";

	order3.xid = "zb";
	order3.oid = "order_3";
	order3.uid = "toiletduck";

	order4.xid = "okex";
	order4.oid = "order_1";
	order4.uid = "gaolj";

	engineData.insertOrder(order1);
	engineData.insertOrder(order2);
	engineData.insertOrder(order3);
	engineData.insertOrder(order4);
	engineData.insertOrder(order4);

	Trade trade1, trade2, trade3, trade4;
	trade1.xid = "zb";
	trade1.oid = "order_1";
	trade1.tid = "trade_1";

	trade2.xid = "zb";
	trade2.oid = "order_1";
	trade2.tid = "trade_2";

	trade3.xid = "zb";
	trade3.oid = "order_2";
	trade3.tid = "trade_3";

	trade4.xid = "zb";
	trade4.oid = "order_3";
	trade4.tid = "trade_4";

	engineData.insertTrade(trade1);
	engineData.insertTrade(trade2);
	engineData.insertTrade(trade3);
	engineData.insertTrade(trade4);
	engineData.insertTrade(trade4);

	auto accounts = engineData.getAccounts("zb");
	engineData.loadData();
}

