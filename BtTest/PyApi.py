﻿# -*- coding: utf-8 -*-
import os
import psutil
import time
import asyncio
import threading
import ccxt.async_support as ccxt
import PyApiCb as callback
import PyApiImpl as impl
import PyApiImpl2 as impl2
import PyForex as forex
import logging
logging.basicConfig(filename="ccxt.log", filemode="w", level=logging.INFO, format='%(asctime)s  %(thread)x  %(threadName)s  %(levelname)s   %(funcName)s    %(message)s')

err_len = 200
all_exchanges = {}

main_loop = asyncio.get_event_loop()
event_loop = asyncio.new_event_loop()
thread = threading.Thread(target=event_loop.run_forever)
thread.daemon = True


def getAllExchanges():
    for id in ccxt.exchanges:
        exchange = getattr(ccxt, id)({
            'asyncio_loop':event_loop,
            'enableRateLimit': True,
        })
        exchange.asyncio_loop2 = exchange.asyncio_loop
        exchange.session2 = exchange.session
        exchange.session = None     # ***

        exchange.asyncio_loop = main_loop
        exchange.open()

        exchange.asyncio_loop, exchange.asyncio_loop2 = exchange.asyncio_loop2, exchange.asyncio_loop
        exchange.session, exchange.session2 = exchange.session2, exchange.session

        all_exchanges[id] = exchange
    return list(all_exchanges.values())

def setExchangeConfig(xid, config):
    for (k, v) in config.items():
        setattr(all_exchanges[xid], k, v)


def setSelectSymbols(xid, symbols):
    valid = []
    exchange = all_exchanges[xid]
    for symbol in symbols:
        if symbol in exchange.symbols:
            valid.append(symbol)
        else:
            logging.warning('%s has not %s', xid, symbol)

    if len(valid) == 0:
        valid = exchange.symbols
    impl2.sel_symbols[xid] = valid
    logging.info('%s    %s', xid, ','.join(impl2.sel_symbols[xid]))


def loadMarkets(xid):
    logging.info('BGN %s', xid)
    exchange = all_exchanges[xid]
    asyncio.run_coroutine_threadsafe(impl2.loadMarkets(exchange), loop=event_loop)


class Account:
    def __init__(self, xid, uid, apiKey = '', secret = ''):
        self.xid = xid
        self.uid = uid
        self.apiKey = apiKey
        self.secret = secret

accounts = {}

def setApikey(xid, uid, apiKey, secret):
    key = xid + uid
    account = Account(xid, uid, apiKey, secret)
    accounts[key] = account


def setExchangeApikey(xid, uid):
    key = xid + uid
    if key not in accounts:
        return None
    account = accounts[key]
    exchange = all_exchanges[xid]
    exchange.apiKey = account.apiKey
    exchange.secret = account.secret
    return exchange

def fetchBalance(xid, uid):
    logging.info('BGN %s', xid)
    exchange = setExchangeApikey(xid, uid)
    if exchange == None:
        logging.error('END %s   %s has not apiKey', xid, uid)
        return
    asyncio.run_coroutine_threadsafe(impl.fetchBalance(exchange, uid), loop=event_loop)


def stopTickers(xid):
    impl2.stop_tickers.add(xid)
def stopOrderBook(xid, symbol):
    if xid in impl2.stop_orderbook:
        impl2.stop_orderbook[xid].add(symbol)
    else:
        impl2.stop_orderbook[xid] = set([symbol, ])


def fetchTickers(xid):
    logging.info('BGN %s', xid)
    if xid in impl2.stop_tickers:
        impl2.stop_tickers.remove(xid)

    exchange = all_exchanges[xid]
    if exchange.has['fetchTickers']:
        asyncio.run_coroutine_threadsafe(impl2.fetchTickers(exchange), loop=event_loop)
    elif exchange.has['fetchTicker']:
        asyncio.run_coroutine_threadsafe(impl2.fetchTicker(exchange), loop=event_loop)


def fetchOrderBook(xid, symbol):
    logging.info('BGN %s    %s', xid, symbol)
    if xid in impl2.stop_orderbook:
        if symbol in impl2.stop_orderbook[xid]:
            impl2.stop_orderbook[xid].remove(symbol)

    exchange = all_exchanges[xid]
    future = asyncio.run_coroutine_threadsafe(impl2.fetchOrderBook(exchange, symbol), loop=event_loop)


def changeEventLoop(exchange):
    exchange.asyncio_loop, exchange.asyncio_loop2 = exchange.asyncio_loop2, exchange.asyncio_loop
    exchange.session, exchange.session2 = exchange.session2, exchange.session
    exchange.enableRateLimit = False


def createOrder(orderReq):
    logging.info('BGN %s', orderReq)
    exchange = setExchangeApikey(orderReq.xid, orderReq.uid)
    if exchange == None:
        logging.error('END %s   %s has not apiKey', orderReq.xid, orderReq.uid)
        return
    changeEventLoop(exchange)

    coro = impl.createOrder(exchange, orderReq)
    main_loop.run_until_complete(coro)


def cancelOrder(xid, uid, oid, symbol=None, params={}):
    logging.info('BGN %s    %s  %s', xid, oid, symbol)
    exchange = setExchangeApikey(xid, uid)
    if exchange == None:
        logging.error('END %s   %s has not apiKey', xid, uid)
        return
    changeEventLoop(exchange)

    coro = impl.cancelOrder(exchange, uid, oid, symbol, params)
    main_loop.run_until_complete(coro)


def fetchOrder(xid, uid, oid, symbol=None, params={}):
    logging.info('BGN %s    %s  %s', xid, oid, symbol)
    exchange = setExchangeApikey(xid, uid)
    if exchange == None:
        logging.error('END %s   %s has not apiKey', xid, uid)
        return
    asyncio.run_coroutine_threadsafe(impl.fetchOrder(exchange, uid, oid, symbol, params), loop=event_loop)


def fetchOrders(func, xid, uid, symbol=None, since=None, limit=None, params={}):
    logging.info('BGN %s    %s  %s', func, xid, symbol)
    exchange = setExchangeApikey(xid, uid)
    if exchange == None:
        logging.error('END %s   %s has not apiKey', xid, uid)
        return
    coro = impl.fetchOrders(func, exchange, uid, symbol, since, limit, params)
    asyncio.run_coroutine_threadsafe(coro, loop=event_loop)


def fetchTradingFees(xid):
    logging.info('BGN %s', xid)
    exchange = all_exchanges[xid]
    asyncio.run_coroutine_threadsafe(impl.fetchTradingFees(exchange), loop=event_loop)


def fetchFundingFees(xid):
    logging.info('BGN %s', xid)
    exchange = all_exchanges[xid]
    asyncio.run_coroutine_threadsafe(impl.fetchFundingFees(exchange), loop=event_loop)


def fetchForex1forge(pairs=[]):
    asyncio.run_coroutine_threadsafe(forex.fetch_forex_1forge(pairs), loop=event_loop)


def fetchForexHexun(pairs=[]):
    asyncio.run_coroutine_threadsafe(forex.fetch_forex_hexun(), loop=event_loop)


async def closeExchanges():
    for exchange in all_exchanges.values():
        await exchange.close()


if __name__ == '__main__':
    process_name = psutil.Process(os.getpid()).name()
    if process_name != 'python.exe':
        logging.info('***exit main***    %s', process_name)
        thread.start()
    else:
        logging.info('***enter main***    %s', process_name)

        getAllExchanges()
        callback.setExchanges(all_exchanges)

        setApikey('zb', 'gaolj', '5dbc8cab-7a14-47ce-9716-dd4c7e127464', '4124841b-c3ea-477d-af8b-8a3e5aedaf78')
        setApikey('bitforex', 'gaolj', '5dbc8cab-7a14-47ce-9716-dd4c7e127464', '4124841b-c3ea-477d-af8b-8a3e5aedaf78')

        xid = 'zb'
        exchange = all_exchanges[xid]

        event_loop.run_until_complete(impl2.loadMarkets(exchange))
        thread.start()

        # fetchBalance(xid, 'gaolj')
        # fetchBalance('bitforex', 'gaolj')

        # setSelectSymbols('zb', ['BTC/USTD' ,'DASH/BTC',])
        # fetchTickers(xid)
        # time.sleep(5)
        # stopTickers(xid)
        #
        # fetchOrderBook(xid, 'DASH/BTC')
        # time.sleep(3)
        # fetchOrderBook(xid, 'ETH/BTC')
        # time.sleep(5)
        # stopOrderBook(xid, 'ETH/BTC')
        # time.sleep(3)
        # stopOrderBook(xid, 'DASH/BTC')

        orderReq = callback.OrderReq()
        orderReq.pid = '123-456-789'
        orderReq.part = 0
        orderReq.xid = xid
        orderReq.uid = 'gaolj'
        orderReq.symbol = 'DASH/BTC'
        orderReq.type = 'limit'
        orderReq.side = 'sell'
        orderReq.amount = 0.01
        orderReq.price = 0.3

        createOrder(orderReq)
        # cancelOrder(xid, 'gaolj', '2018122563640658', 'DASH/BTC')
        # fetchOrder(xid, 'gaolj', '2018122563640658', 'DASH/BTC')
        #
        # fetchOrders('fetchOrders', xid, 'gaolj', 'DASH/BTC')
        # fetchOrders('fetchOpenOrders', xid, 'gaolj', 'DASH/BTC')
        # fetchOrders('fetchClosedOrders', xid, 'gaolj', 'DASH/BTC')
        # fetchOrders('fetchMyTrades', xid, 'gaolj', 'DASH/BTC')
        # fetchOrders('fetchTrades', xid, 'gaolj', 'DASH/BTC')

        # fetchForexHexun()
        # fetchForex1forge()
        # fetchTradingFees(xid)
        tasks = asyncio.Task.all_tasks(event_loop)
        for task in tasks:
            while not task.done():
                time.sleep(1)

        asyncio.run_coroutine_threadsafe(closeExchanges(), loop=event_loop)
        tasks = asyncio.Task.all_tasks(event_loop)
        for task in tasks:
            while not task.done():
                time.sleep(1)
