﻿# -*- coding: utf-8 -*-
import time
import datetime
import json
import aiohttp
import PyApiCb as callback
import logging
err_len = 200


async def fetch_forex_1forge(pairs=[]):
    api_key = '&api_key=vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2'
    url_1forge = 'http://forex.1forge.com/1.0.3/quotes?pairs=' + ','.join(pairs) + api_key
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url_1forge) as response:
                forexs = await response.json()
                for forex in forexs:
                    dt = datetime.datetime.fromtimestamp(forex['timestamp'])
                    forex['datetime'] = dt.strftime('%Y-%m-%d %H:%M:%S')
                callback.fetchForexCb('1forge', forexs)
    except Exception as e:
        logging.error('%s   %s', type(e).__name__, str(e) if len(str(e)) < err_len else str(e)[:err_len])


async def fetch_forex_hexun():
    url_hexun = 'http://forex.wiapi.hexun.com/forex/sortlist?block=303&title=15&column=code,price,datetime'
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url_hexun) as response:
                body = await response.text()
                quotes = json.loads(body[1 : len(body) - 2])
                forexs = []
                for quote in quotes['Data'][0]:
                    forex = {}
                    forex['symbol'] = quote[0]
                    forex['price'] = quote[1] / 10000
                    dt = datetime.datetime.strptime(str(quote[2]), '%Y%m%d%H%M%S')
                    forex['timestamp'] = int(time.mktime(dt.timetuple()))
                    forex['datetime'] = dt.strftime('%Y-%m-%d %H:%M:%S')
                    forexs.append(forex)
                callback.fetchForexCb('hexun', forexs)
    except Exception as e:
        logging.error('%s   %s', type(e).__name__, str(e) if len(str(e)) < err_len else str(e)[:err_len])


