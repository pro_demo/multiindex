#pragma once
#include <map>
#include <string>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
using boost::multi_index_container;
using namespace boost::multi_index;

struct Balance
{
	std::map<std::string, double> free;
	std::map<std::string, double> used;
	std::map<std::string, double> total;
};

struct Symbol
{
	std::string xid;
	std::string id;		// BTC/USDT, BTC/USD
	std::string base;
	std::string quote;
	bool selected = false;
};

struct Account
{
	int did = 0;			// local db id
	std::string xid;		// exhange id
	std::string uid;		// user id
	std::string password;
	std::string accessKey;
	std::string secretKey;
	Balance balance;
	bool islogin = false;
	std::string errorMsg;
};

typedef multi_index_container<
	Account,
	indexed_by<
		ordered_unique<
			composite_key<
				Account,
				member<Account, std::string, &Account::xid>,
				member<Account, std::string, &Account::uid>
			>
		>
	>
> AccountSet;
