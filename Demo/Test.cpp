#include "Test.h"
#include "Coin.h"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/locale.hpp>		// boost::locale::conv::from_utf
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include "json.hpp"
#include "fifo_map.hpp"
#include "Symbol.h"

template<class K, class V, class dummy_compare, class A>
using my_workaround_fifo_map = nlohmann::fifo_map<K, V, nlohmann::fifo_map_compare<K>, A>;
using json = nlohmann::basic_json<my_workaround_fifo_map>;
//using json = nlohmann::json;

void Serialization(CoinSet &coins)
{
	std::string file = "debug/store.txt";
	std::ofstream ofs(file);
	boost::archive::binary_oarchive oa(ofs);
	oa << coins;
	ofs.close();

	coins.clear();
	std::ifstream ifs(file);
	if (ifs) {
		boost::archive::binary_iarchive ia(ifs);
		ia >> coins;
	}

}

void CoinSetting()
{
	CoinSet _coins;
	json gCoinSetting;
	try
	{
		std::ifstream ifs("Coin.setting");
		ifs >> gCoinSetting;

		int rank = 1001;
		std::set<Coin> coinsName;
		auto &fiatName = gCoinSetting["fiat"];
		for (auto it = fiatName.begin(); it != fiatName.end(); ++it)
		{
			auto value = it.value().get<std::string>();
			Coin coin(it.key(), COIN_FIAT);
			coin.rank = rank++;
			coin.chinese = boost::locale::conv::from_utf(value, "gb2312");
			_coins.insert(coin);
		}

		auto &baseCoin = gCoinSetting["baseCoin"];
		for (auto it = baseCoin.begin(); it != baseCoin.end(); ++it)
			_coins.insert(Coin(it.value().get<std::string>(), COIN_BASE));

		auto &stableCoin = gCoinSetting["stableCoin"];
		for (auto it = stableCoin.begin(); it != stableCoin.end(); ++it)
			_coins.insert(Coin(it.value().get<std::string>(), COIN_STABLE));

		auto &checked = gCoinSetting["checked"];
		for (auto it = checked.begin(); it != checked.end(); ++it)
		{
			auto id = it.value().get<std::string>();
			auto itCoin = _coins.find(id);
			if (itCoin != _coins.end())
				_coins.modify(itCoin, ChangeSelected(true));
			else
				_coins.insert(Coin(id, COIN_CRYPTO, true));
		}

		std::vector<std::string> vec = { "DASH", "BTC" };
		vec.push_back("USD");
		gCoinSetting["checked"] = vec;
		std::ofstream o("Coin.setting");
		o << std::setw(4) << gCoinSetting << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what();
	}
}
void jsonTest()
{
	{
		std::set<std::string> c_set{ "one", "two", "three", "four", "one" };
		json j_set(c_set);

		auto dmp = j_set.dump();

		json j = json::parse(dmp);
		auto dmp2 = j.dump();

		c_set.clear();
		c_set = j.get<std::set<std::string>>();

		for (auto it = j.begin(); it != j.end(); it++)
		{
			auto a = it.value().get<std::string>();
			c_set.insert(a);
		}
	}
	
	
	{
		json j;
		j["pi"] = 3.141;
		j["happy"] = true;
		j["name"] = "Niels";
		j["nothing"] = nullptr;
		j["answer"]["everything"] = 42;
		j["list"] = { 1, 0, 2 };
		j["object"] = { { "currency", "USD" },{ "value", 42.99 } };
		std::ofstream o("debug/Coin.txt");
		o << std::setw(4)  << j << std::endl;
	}
	{
		nlohmann::json j = {
			{ "pi", 3.1415926 },
			{ "happy", false },
			{ "name", "Niels" },
			{ "nothing", nullptr },
			{ "answer",{
				{ "everything", 42 }
			} },
			{ "list",{ 1, 0, 2 } },
			{ "object",{
				{ "currency", "USD" },
				{ "value", 42.99 }
			} }
		};
		std::ofstream o("debug/Coin.txt");
		o << std::setw(4) << j << std::endl;
	}
	{
		json j_string = "this is a string";
		auto type = j_string.type();
		auto cpp_string = j_string.get<std::string>();
		std::string cpp_string2;
		j_string.get_to(cpp_string2);

		// retrieve the serialized value (explicit JSON serialization)
		std::string serialized_string = j_string.dump();
	}
	{
		std::vector<std::uint8_t> v1 = { 't', 'r', 'u', 'e' };
		json j;
		j = json::parse(v1.begin(), v1.end());
		j = json::parse(std::string("123.45"));
		//j = json::parse(std::string("abcde"));��
		j = json::parse(std::string("false"));
		//std::vector<std::uint8_t> v2 = { 'a', 'r', 'u', 'e' };��
		//std::vector<std::string> v3 = { "aaa", "bbb", "ccc" };��
	}
	{
		json j;
		j.push_back("foo");
		j.push_back(1);
		j.push_back(true);
		j.emplace_back(1.78);

		for (json::iterator it = j.begin(); it != j.end(); ++it) {
			std::cout << *it << '\n';
		}
		for (auto& element : j) {
			std::cout << element << '\n';
		}
		std::ofstream s("debug/Coin.txt");
		s << std::setw(4) << j << std::endl;
	}
}

std::ostream& operator<<(std::ostream& os, const Account& account)
{
	os << account.did << "\t" << account.xid << "\t" << account.uid << std::endl;
	return os;
}

void AccountTest()
{
	AccountSet accounts;

	Account account1, account2, account3;
	account1.xid = "zb";
	account1.uid = "gaolj";

	account2.xid = "zb";
	account2.uid = "toiletduck";

	account3.xid = "zb";
	account3.uid = "caoning";

	accounts.insert(account1);
	accounts.insert(account2);
	accounts.insert(account3);

	auto it = accounts.find(boost::make_tuple("zb", "gaolj"));
	std::cout << *it;
	auto iter = accounts.equal_range("zb");
	while (iter.first != iter.second)
		std::cout << *iter.first++;
}