#include "Coin.h"
#include "Ticker.h"
#include "Symbol.h"
#include <iostream>
#include <boost/lambda/lambda.hpp>
#include "Test.h"
#include "Symbol.h"

#if !defined(NDEBUG)
#define BOOST_MULTI_INDEX_ENABLE_INVARIANT_CHECKING
#define BOOST_MULTI_INDEX_ENABLE_SAFE_MODE
#endif

template<typename Tag,typename MultiIndexContainer>
void print_out_by(const MultiIndexContainer& s)
{
  /* obtain a reference to the index tagged by Tag */
  const typename boost::multi_index::index<MultiIndexContainer,Tag>::type& i=
    get<Tag>(s);

  typedef typename MultiIndexContainer::value_type value_type;

  /* dump the elements of the index to cout */
  std::copy(i.begin(),i.end(),std::ostream_iterator<value_type>(std::cout));
}

std::ostream& operator<<(std::ostream& os, const Coin& coin)
{
	std::string str;
	for (auto & exchange : coin.exchanges)
		str += exchange;
	os << coin.id << "\t字母:" << coin .id[0] << "\t选择:" << coin.selected << "\t类型:" << coin.type << "\t:rank" << coin.rank << "\t交易所:" << str << std::endl;
	return os;
}
std::ostream& operator<<(std::ostream& os, const Ticker& ticker)
{
	os << ticker.xid << "\t" << ticker.symbol << "\t" << ticker.last << std::endl;
	return os;
}

int main()
{
	AccountTest();
	jsonTest();
	CoinSetting();
	CoinSet coins;

	Coin coin("ETH", COIN_BASE);
	coin.id = "ETH";
	coin.type = COIN_BASE;
	coin.selected = true;
	coin.rank = 1010;
	coin.exchanges.insert("x1");
	coins.insert(coin);

	coin.id = "USDT";
	coin.type = COIN_STABLE;
	coin.selected = false;
	coin.rank = 1001;
	coin.exchanges.insert("x1");
	coin.exchanges.insert("x2");
	coins.insert(coin);

	coin.id = "BTC";
	coin.type = COIN_BASE;
	coin.selected = false;
	coin.exchanges.insert("x1");
	coin.exchanges.insert("x2");
	coin.exchanges.insert("x3");
	coins.insert(coin);

	coin.id = "USD";
	coin.type = COIN_FIAT;
	coin.selected = false;
	coin.rank = 1000;
	coins.insert(coin);
	{
		Coin coin;
		coin.id = "BTX";
		coin.type = COIN_CRYPTO;
		coin.selected = true;
		coin.rank = 1003;
		coins.insert(coin);
	}

	{
		auto it = coins.find("BTC");
		if (it != coins.end())
		{
			auto temp = *it;
			temp.exchanges.insert("x3");
			temp.rank = 999;
			coins.replace(it, temp);
		}
	}

	Serialization(coins);

	{
		std::cout << "byrank: >=1000" << std::endl;
		auto &rankSet = coins.get<byrank>();
		auto itpair  = rankSet.range(1000 <= boost::lambda::_1, unbounded);
		while (itpair.first != itpair.second)
			std::cout << *itpair.first++;

		std::cout << "bytype:COIN_CRYPTO, BTX" << std::endl;
		CoinSet::index<bytype>::type &coinByType = coins.get<bytype>();
		auto it = coinByType.find(boost::make_tuple(COIN_CRYPTO, "BTX"));
		if (it != coinByType.end())
			std::cout << *it;

		std::cout << "byletter" << std::endl;
		auto &coinByletter = coins.get<byletter>();
		for (auto &coin : coinByletter)
			std::cout << coin;
		std::cout << "byletter equal_range(B)" << std::endl;
		auto p = coinByletter.equal_range('B' - 65);
		while (p.first != p.second)
			std::cout << *p.first++;

		std::cout << "byxnum" << std::endl;
		CoinSet _set;
		auto &setByxnum = coins.get<byxnum>();
		auto len = setByxnum.rbegin()->exchanges.size();
		len = setByxnum.begin()->exchanges.size();
		auto pair = setByxnum.range(3 <= boost::lambda::_1, unbounded);
		while (pair.first != pair.second)
			_set.insert(*pair.first++);

		for (auto &coin : _set)
			std::cout << coin;

	}
	{
		std::cout << "byselected" << std::endl;
		auto &coinByselected = coins.get<byselected>();
		auto it = coinByselected.find(boost::make_tuple(true, "ETH"));
		if (it != coinByselected.end())
		{
			std::cout << *it;
			coinByselected.modify(it, ChangeSelected(false));
			std::cout << *it;
		}
		std::cout << std::endl;
		auto pair = coinByselected.equal_range(true);
		while (pair.first != pair.second)
			std::cout << *pair.first++;
	}

	std::cout << "byid" << std::endl;
	for (auto &coin : coins)
		std::cout << coin;
	std::cout << "byletter" << std::endl;
	for (auto &coin : coins.get<byletter>())
		std::cout << coin;
	std::cout << "bytype" << std::endl;
	for (auto &coin : coins.get<bytype>())
		std::cout << coin;
	std::cout << "byselected" << std::endl;
	for (auto &coin : coins.get<byselected>())
		std::cout << coin;

	TickerSet tickers;
	int i = tickers.size();
	TickerSetByXid &unique_index = tickers.get<byxid>();
	{
		Ticker ticker("zb", "BTC/USDT", 31);
		auto iter = unique_index.find(boost::make_tuple(ticker.xid, ticker.symbol));
		if (iter == unique_index.end())
			unique_index.insert(ticker);
		else
			unique_index.replace(iter, ticker);
	}
	{
		Ticker ticker("zb", "BTC/USDT", 38);
		auto iter = unique_index.find(boost::make_tuple(ticker.xid, ticker.symbol));
		if (iter == unique_index.end())
			unique_index.insert(ticker);
		else
			unique_index.replace(iter, ticker);
	}
	{
		Ticker ticker("zb", "BTC/USDT", 33);
		auto iter = tickers.find(boost::make_tuple(ticker.xid, ticker.symbol));
		if (iter == unique_index.end())
			tickers.insert(ticker);
		else
			tickers.replace(iter, ticker);
	}

	auto it = tickers.insert(Ticker("zb", "BTC/USDT", 27));
	it = tickers.insert(Ticker("zb", "LTC/USDT", 27));
	it = tickers.insert(Ticker("zb", "LTC/BTC", 27));
	it = tickers.insert(Ticker("okex", "BTC/USDT", 40));
	it = tickers.insert(Ticker("okex", "BTC/ETH", 40));
	it = tickers.insert(Ticker("okex", "BTC/USDT", 40));
	it = tickers.insert(Ticker("x1", "BTC/USDT", 40));
	it = tickers.insert(Ticker("x2", "BTC/USDT", 41));
	it = tickers.insert(Ticker("x3", "BTC/USDT", 42));

	std::cout << "***byxid" << std::endl;
	//for (auto &ticker : tickers.get<byxid>())	// 同下面效果一样
	for (auto &ticker : tickers)
		std::cout << ticker;
	std::cout << "***bysym" << std::endl;
	for (auto &ticker : tickers.get<bysym>())
		std::cout << ticker;

	std::cout << "byxid" << std::endl;
	print_out_by<byxid>(tickers);
	std::cout << std::endl;

	std::cout<<"bysym"<<std::endl;
	print_out_by<bysym>(tickers);
	std::cout<<std::endl;

	auto iter = tickers.equal_range("zb");
	auto iter2 = iter;
	while (iter.first != iter.second)
		std::cout << *iter.first++;

	tickers.erase(iter2.first, iter2.second);
	iter = tickers.equal_range("zb");
	while (iter.first != iter.second)
		std::cout << *iter.first++;

	char c;
	std::cin >> c;
	return 1;
}
