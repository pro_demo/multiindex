#pragma once
#include <string>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/mem_fun.hpp>
using boost::multi_index_container;
using namespace boost::multi_index;

struct Ticker
{
	Ticker(std::string id, std::string smb, double l) :xid(id), symbol(smb), last(l) {}
	std::string xid;	// exchange id
	std::string symbol;
	double last = 0;
};
struct byxid {};
struct bysym {};
struct xid_symbol : public composite_key<
	Ticker,
	BOOST_MULTI_INDEX_MEMBER(Ticker, std::string, xid),
	BOOST_MULTI_INDEX_MEMBER(Ticker, std::string, symbol)
> {};
struct symbol_price : public composite_key<
	Ticker,
	BOOST_MULTI_INDEX_MEMBER(Ticker, std::string, symbol),
	BOOST_MULTI_INDEX_MEMBER(Ticker, double, last)
> {};

typedef multi_index_container
<
	Ticker,
	indexed_by
	<
		ordered_unique<tag<byxid>, xid_symbol>,		// ordered by xid & symbol, or ???hashed speed find
		ordered_non_unique<tag<bysym>, symbol_price>// ordered by symbol & price
	>
> TickerSet;

typedef TickerSet::index<byxid>::type TickerSetByXid;
typedef TickerSet::index<bysym>::type TickerSetBySymbol;


