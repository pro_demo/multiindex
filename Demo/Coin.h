#pragma once
#include <set>
#include <string>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/mem_fun.hpp>
using boost::multi_index_container;
using namespace boost::multi_index;


#define COIN_FIAT		0x000000001		// 法币
#define COIN_BASE		0x000000002		// 基础加密货币
#define COIN_STABLE		0x000000004		// 稳定加密货币
#define COIN_CRYPTO		0x000000008		// 普通加密货币
struct Coin
{
	Coin(std::string id = "XXX") : id(id) {}
	Coin(std::string id, int type) : id(id), type(type) {}
	Coin(std::string id, int type, bool selected) : id(id), type(type), selected(selected) {}

	std::string id;			// BTC, ETH, USD, EUR, 
	std::string name;		// Bitcoin, Ethereum, US Dollar, Euro
	std::string chinese;	// 中文名：比特币, 美元

	int type = COIN_CRYPTO;	// COIN_FIAT
	bool selected = false;	// 是否被选择

	int rank = 0;			// 排名
	double price = 0;		// 价格：USD
	double volume = 0;		// 24小时交易量
	std::string change;		// 24小时价格涨跌幅:1.2%

	std::set<std::string> exchanges;	// 有此货币的交易所

	int ExchangeNum()const { return exchanges.size(); }
	char FirstLetter()const { if (isalpha(id[0])) return (id[0] - 65); else return 27; }
	bool operator < (const Coin &other) const
	{
		return id < other.id;
	}
};
class CoinCompByRank
{
public:
	bool operator()(const Coin& left, const Coin& right) const
	{
		return left.rank < right.rank;
	}
};
struct ChangeSelected
{
	ChangeSelected(bool b) :_b(b) {}
	void operator()(Coin& coin) { coin.selected = _b; }
private:
	bool _b;
};
struct bytype {};
struct byrank {};
struct byxnum {};
struct byletter {};
struct byselected {};
typedef multi_index_container<
	Coin,
	indexed_by<
		hashed_unique<member<Coin, std::string, &Coin::id>>,	// 按id, hashed排序
		ordered_non_unique<	// 按排名rank
			tag<byrank>,
			member<Coin, int, &Coin::rank>
		>,
		ordered_non_unique<	// 按交易所数
			tag<byxnum>,
			const_mem_fun<Coin, int, &Coin::ExchangeNum>
		>,
		ordered_non_unique<	// 按首字母letter
			tag<byletter>,
			composite_key<
				Coin,
				const_mem_fun<Coin, char, &Coin::FirstLetter>,
				member<Coin, std::string, &Coin::id>
			>
		>,
		ordered_non_unique<	// 按类型type
			tag<bytype>,
			composite_key<
				Coin,
				member<Coin, int, &Coin::type>,
				member<Coin, std::string, &Coin::id>
			>
		>,
		ordered_non_unique<	// 按被选择selected
			tag<byselected>,
			composite_key<
				Coin,
				member<Coin, bool, &Coin::selected>,
				member<Coin, std::string, &Coin::id>
			>
		>
	>
> CoinSet;

#include <boost/serialization/set.hpp>
namespace boost {
	namespace serialization {
		template<class Archive>
		void serialize(Archive & ar, Coin & param, const unsigned int version)
		{
			ar & param.id;
			ar & param.name;
			ar & param.chinese;
			ar & param.type;
			ar & param.selected;
			ar & param.rank;
			ar & param.price;
			ar & param.volume;
			ar & param.change;
			ar & param.exchanges;
		}
	} // namespace serialization
} // namespace boost


